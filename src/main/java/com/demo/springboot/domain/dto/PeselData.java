package com.demo.springboot.domain.dto;

import com.demo.springboot.validators.PeselValidator;

public class PeselData {
    private String pesel;

    public PeselData(String pesel) {
        this.pesel = pesel;
    }

    @Override
    public String toString() {
        return "{\"PeselData\":{"
                + "\"pesel:\":\"" + pesel
                + "}}";
    }

    public boolean isPeselCorrect() {
        PeselValidator validator = new PeselValidator(pesel);
        if (validator.isValid() == true) {
            return true;
        } else {
            return false;
        }
    }
}
