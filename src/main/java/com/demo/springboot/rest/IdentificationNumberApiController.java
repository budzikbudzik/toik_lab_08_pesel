package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.PeselData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class IdentificationNumberApiController {
    private static final Logger LOGGER = LoggerFactory.getLogger(IdentificationNumberApiController.class);

    @CrossOrigin
    @GetMapping(value = "/check-identification-number")
    public ResponseEntity<Void> checkIdentificationNumber(@RequestParam(defaultValue = "") String id) {

        try {
            PeselData myPesel = new PeselData(id);
            boolean wynik = myPesel.isPeselCorrect();
            LOGGER.info("--- check identification number: {}, {}", id, wynik);

            if (myPesel.isPeselCorrect() == true) {
                LOGGER.info("Pesel" + id + " jest poprawny.");
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                LOGGER.info("Pesel " + id + " jest niepoprawny.");
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}


